# DockerfileFromImage #

Small script to create an Dockerfile from unknown image of the Docker

* Version 0.1
### How to use ###



* Clone this repo
* Copy in /

```
#!shell

chmod +x /usr/local/bin/dockhist
```
* See your run containers 
```
#!shell
docker ps
```

* Then type **dockhist** and name of the container
* For example
```
#!shell

dockhist postgres
```
Displays

```
#!shell

ENV POSTGRES_PASSWORD=psqlpasswd123

ENV POSTGRES_USER=postgres

ENV POSTGRES_DB=grafana

ENV PATH=/usr/lib/postgresql/9.5/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

ENV LANG=en_US.utf8

ENV PG_MAJOR=9.5

ENV PG_VERSION=9.5.0-1.pgdg80+2

ENV PGDATA=/var/lib/postgresql/data

EXPOSE 5432/tcp

VOLUME /var/lib/postgresql/data


ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["postgres"]
```
